{-# LANGUAGE OverloadedStrings #-}
--
module Jaiyalas.ShanShan.Bot where
--
import Data.Aeson
import Network.HTTP.Simple
import qualified Data.Text as T
import Data.HashMap.Strict (fromList)
--
tgtoken = "1115925979:AAGBYuwu24tiJrG0RfWACevKuGf3BvDpcyQ"
--
--
telegramMsgJSON :: String -> Value
telegramMsgJSON msg = Object $ fromList $
    [ ("chat_id", String "-409245406")
    , ("disable_web_page_preview", Bool True)
    , ("disable_notification", Bool False)
    -- , ("parse_mode", String "MarkdownV2")
    , ("text", String $ T.pack msg)
    ]
--
postMsg2ssn :: String -> IO Int
postMsg2ssn msg = do
  let sendMsgURL = "https://api.telegram.org/bot"++tgtoken++"/sendMessage"
  initReq <- parseRequest sendMsgURL
  let req = Prelude.id
          $ setRequestMethod "POST"
          $ setRequestBodyJSON (telegramMsgJSON msg)
          $ initReq
  rs <- httpLBS req
  return $ getResponseStatusCode rs
