{-# LANGUAGE OverloadedStrings #-}
-- -- -- -- -- -- -- -- -- -- -- -- -- --
module Jaiyalas.Leech where
-- -- -- -- -- -- -- -- -- -- -- -- -- --
import           Control.Monad
-- ---           ----------------------
import           Network.HTTP.Simple
import           Network.HTTP.Client
import           Network.Curl
-- ---           ----------------------
import           Text.HTML.TagSoup
import           Text.StringLike
import           Text.Regex.TDFA
-- ---           ----------------------
import           Data.Aeson
import           Data.Aeson.Types
import qualified Data.ByteString.Lazy as BS
import qualified Data.Text as T
import           Data.Time
-- ---           ----------------------
import qualified Codec.Text.IConv as ICONV
-- ---           ----------------------
import           Jaiyalas.Leech.JSON
import           Jaiyalas.Leech.URLs
-- -- -- -- -- -- -- -- -- -- -- -- -- --
--
--
{- ===== ===== YAHOO FINANCE ===== ===== -}
--
getMsgYahoo :: String -> IO String
getMsgYahoo goodName = do
  rs <- (httpLBS =<< parseRequest (yfURL ++ goodName))
  let tags = parseTags $ getResponseBody rs
  let name = fullnameTrimmer $ tags2FullName tags
      curr = currencyTrimmer $ tags2Currency tags
      pric = priceTrimmer $ tags2Price tags
  let msg = name ++ " = " ++ pric ++ " (" ++ curr ++ ")"
  return msg
--
tags2FullName :: (Show a, StringLike a) => [Tag a] -> String
tags2FullName tags = show $ fromTagText $ (!! 1) $ head $
  sections (~== TagOpen ("h1" :: String) [("data-reactid" :: String,"7" :: String)]) tags
--
tags2Currency :: (Show a, StringLike a) => [Tag a] -> String
tags2Currency tags = show $ fromTagText $ (!! 1) $ head $
  sections (~== TagOpen ("span" :: String) [("data-reactid" :: String,"9" :: String)]) tags
--
tags2Price :: (Show a, StringLike a) => [Tag a] -> String
tags2Price tags = show $ fromTagText $ (!! 1) $ head $
  sections (~== TagOpen ("span" :: String) [("data-reactid" :: String,"32" :: String)]) tags
--
fullnameTrimmer :: String -> String
currencyTrimmer :: String -> String
priceTrimmer :: String -> String
--
fullnameTrimmer str = fmap (rmDot) $ tail $ init $ init $ last $ words str
currencyTrimmer str = init $ last $ words str
priceTrimmer    str = tail $ init $ str
--
rmDot :: Char -> Char
rmDot '.' = '_'
rmDot  a  = a
--
--

