module Jaiyalas.Leech.JSON where
--
import Data.Aeson
import Data.ByteString.Lazy
--
import Network.HTTP.Simple
--
import qualified Codec.Text.IConv as ICONV
--


parseValue' :: ByteString -> Either String Value
parseValue' rs = eitherDecode rs
--  case (eitherDecode rs) of
--    (Right v) -> Right v
--    (Left ms) -> eitherDecode $ (ICONV.convert "BIG5" "UTF-8") rs 
--
--
--
parseValue :: Response ByteString -> Either String Value
parseValue rs =
  case (getResponseStatusCode rs, eDecode rs) of
    (200, Right v) -> Right v
    (200, Left ms) -> eDecodeBig5 rs 
    (n, _)   -> Left ("status code: " ++ (show n)) 
--
eDecode :: Response ByteString -> Either String Value
eDecode rs
    = eitherDecode
    $ getResponseBody rs
--
eDecodeBig5 :: Response ByteString -> Either String Value
eDecodeBig5 rs
    = eitherDecode
    $ (ICONV.convert "BIG5" "UTF-8")
    $ getResponseBody rs
--
