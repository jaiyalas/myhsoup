{-# LANGUAGE OverloadedStrings #-}
-- -- -- -- -- -- -- -- -- -- -- -- -- --
module Jaiyalas.Leech.BFT where
-- -- -- -- -- -- -- -- -- -- -- -- -- --
import           Control.Monad
-- ---           ----------------------
import           Network.HTTP.Simple
import           Network.HTTP.Client
import           Network.Curl
import           Text.Regex.TDFA
-- ---           ----------------------
import           Data.Aeson
import           Data.Aeson.Types
import qualified Data.ByteString.Lazy as BS
import qualified Data.Text as T
-- ---           ----------------------
import qualified Codec.Text.IConv as ICONV
-- ---           ----------------------
import           Jaiyalas.Leech.JSON
import           Jaiyalas.Leech.URLs
-- -- -- -- -- -- -- -- -- -- -- -- -- --
--
--
{- ===== ===== Bureau of Foreign Trade ===== ===== -}
--
getMsgBFT :: IO (Either String [BFTNewsEntity])
getMsgBFT = do
  --
  (cc, bstr) <- curlGetString_ "https://www.trade.gov.tw/Api/Get/pages?nodeid=45&timeRestrict=true" []
  putStrLn "parse start"
  let yo = eitherDecode bstr
  putStrLn "parse done"
  return yo
--
--
data BFTNewsEntity = BFTNewsEntity
  { pt  :: T.Text
  , pps :: T.Text
  , ps  :: T.Text
  } deriving (Eq, Show)
instance FromJSON BFTNewsEntity where
    parseJSON = withObject "" $ \o -> do
      _pt  <- o .: "PageTitle"
      _pps <- o .: "PagePublishTime"
      _ps  <- o .: "PageSummary"
      return $ BFTNewsEntity _pt _pps _ps 
--
--

