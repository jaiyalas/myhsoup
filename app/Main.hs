{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module Main where
--
import Control.Monad
import Data.String
--
-- import Data.Aeson
-- import Data.Aeson.Types
import qualified Data.Text as T
-- import Data.String (words)
-- import qualified Data.ByteString.Lazy as BS 
-- --
import Text.Regex.TDFA
-- -- import qualified Data.ByteString.Char8 as C8 -- C8.unpack
-- --
-- import Network.HTTP.Simple
-- -- import Data.HashMap.Strict (fromList)
-- --
-- import System.IO
-- --
-- import Text.HTML.TagSoup
-- import Text.StringLike
--
-- import GHC.Generics
--
import Data.String
--
import Jaiyalas.Leech
import Jaiyalas.Leech.JSON
import Jaiyalas.Leech.IEAT
import Jaiyalas.Leech.BFT
import Jaiyalas.ShanShan.Bot
--
import Data.Time
import Data.Time.Format.ISO8601
--

getROCyt :: LocalTime -> String
getROCyt t =   
  let d = localDay t 
      y = (read $ formatTime defaultTimeLocale "%Y" d) :: Int
      md = formatTime defaultTimeLocale "%m/%d" d
  in (show (y - 1911) ++ "/" ++ md)


patterns :: String
patterns = "歐盟|美國|法國|農業|食品|衛生|水果|果乾|電商|跨境"

-- 腎臟醫療新知
-- https://www.tsn.org.tw/UI/G/G004.aspx

main :: IO ()
main = do
  tutc     <- getCurrentTime
  tzone    <- getCurrentTimeZone 
  let tloc = utcToLocalTime tzone tutc
  let troc = getROCyt tloc 
  -- ----- -- ----- -- ----- -- ----- --
  putStrLn "Charger les données de Yahoo.."
  ms <-  mapM getMsgYahoo ["0050.TW", "AMZN", "VOO", "VOOV", "VOOG", "QQQ", "TQQQ", "VGT"]
  i0 <- postMsg2ssn $ unlines ms
  putStrLn $ show i0
  -- -- ----- -- ----- -- ----- -- ----- --
  putStrLn "Charger les données de IEAT.."
  ev1 <- getMsgIEAT
  case ev1 of
    Left err -> putStrLn $ "[IEAT] " ++ err
    Right xs -> do 
      let msg1' = unlines
                $ filter (\x -> x =~ (troc :: String))
                $ filter (\o -> o =~ patterns)
                $ map show $ ines xs
      let msg1 = if msg1' == "" then "本日無經貿法規新聞" else msg1'
      i1 <- postMsg2ssn msg1
      putStrLn $ show i1
  -- -- ----- -- ----- -- ----- -- ----- --
  putStrLn "Charger les données de BFT.."
  ev2 <- getMsgBFT
  case ev2 of
    Left err -> putStrLn $ "[BTF] " ++ err
    Right xs -> do
      let msg2' = unlines 
                $ fmap ((++"\n") . T.unpack . pt)
                $ filter ((== [troc]) . liftM getROCyt . (iso8601ParseM :: String -> [LocalTime]) . T.unpack . pps)                
                $ xs 
      let msg2 = if msg2' == "" then "本日無國際貿易新聞" else msg2'
      i2 <- postMsg2ssn msg2
      putStrLn $ show i2
  -- -- ----- -- ----- -- ----- -- ----- --
  putStrLn "Tout est fini !"
--
--  let utct = understandTime "2020-10-19T14:52:00"
--  putStrLn $ formatTime defaultTimeLocale "%Y/%m/%d" utct
understandTime :: String -> UTCTime
understandTime s = parseTimeOrError True defaultTimeLocale "%Y-%m-%dT%H:%M:%S" s
